      TPF0	TTabSheet CaptionGI-TP1-Pag-1-TSL_rogb	PopupMenuFormUsr.PagePopupMenu      TPF0TTlmyViewLabel LeftTopWidth� Height#CaptionMODO OPERATIVOFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder      TPF0TTlmyTxt LeftTop3Width� HeightHint� Operational Mode Code in Text Format:
 Bin: Int: Text
 XXXXXX00 : 0  : SCIENCE
 XXXXXX01 : 1  : PROPULSION
 XXXXXX10 : 2  : SURVIVAL
 XXXXXX11 : 3  : SAFE HOLD

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarNameTlmy::RODE.SMOBC.Mode.TxtSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidthq	UnitWidthdLabelFontColorclBlue      TPF0TTlmyViewLabel LeftTopOWidth� Height#CaptionTRANSMISOR ACTIVOFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder     TPF0TTlmyTxt LeftToptWidth� HeightHint� Active S-band Transmitter Code in Text Format:
 Bin: Int: Text
 XXXXXX00 : 0  : TXS1
 XXXXXX01 : 1  : TXS2
 XXXXXX10 : 2  : ERROR
 XXXXXX11 : 3  : ERROR

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarNameTlmy::RODE.SMTXS.Active.TxtSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidthr	UnitWidthdLabelFontColorclBlue      TPF0TTlmyViewLabel LeftTop� Width� Height#CaptionON BOARD TIMEFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder     TPF0TTlmyTxt LeftTop� Width� HeightHint  On Board Time UTC format

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarName"Tlmy::RODE.SYS.OBT.Time.ReferencedSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidths	UnitWidthdLabelFontColorclBlue      TPF0TTlmyViewLabel LeftTop� Width� Height#Caption	CAST TIMEFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder     TPF0TTlmyTxt LeftTop:Width]HeightHint? Scaled Battery Bus Voltage from Real Format to Limit Voltage

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarName&Tlmy::RODE.PMOBC.Batt.Volt.Real.ScaledAlarmVarName'Alarm::RODE.PMOBC.Batt.Volt.Real.ScaledAlarmEnable	DisplayUnitVSignificantDigitsDisplayFormat	ShowLabel		ShowUnits		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidtht	UnitWidthcLabelFontColorclBlueUnitsFontColorclBlue      TPF0TTlmyViewLabel LeftTopWidth� Height#CursorcrSizeWECaptionTENSI�N Y ALARMAFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder     TPF0TTlmyTxt Left Top� Width� HeightHint  CAS System Time in UTC format

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrder	TlmyVarName"Tlmy::RODE.SYS.CASTime.Time.WOMSECSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidths	UnitWidthdLabelFontColorclBlue      TPF0TTlmyViewLabel LeftTopWWidth� Height#CaptionCONTADORFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont	PopupMenuFormUsr.NonTlmyPopupMenuTabOrder
     TPF0TTlmyTxt LeftTop}Width� HeightHint1 Extracted Frame Counter in Integer Data Format

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarName!Tlmy::RODE.SMOBC.FrameCounter.IntSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidtht	UnitWidthdLabelFontColorclBlue  Blue     TPF0TTlmyTxt Left,Top� Width^HeightHint- On Board Time UTC format in Integer Format

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarName!Tlmy::RODE.SYS.OBT.Time.ExtractedSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidth� 	UnitWidthd  dth^HeightHint  CAS System Time in UTC format

BevelOuterbvNone	PopupMenuFormUsr.ComponentPopupMenuTabOrderTlmyVarNameTlmy::SYS.CASTime.Time.WMSECSignificantDigitsDisplayFormat	ShowLabel		ShowValue	
LabelAlignalLeft
ValueAlignalClient
UnitsAlignalRight
LabelWidth� 
ValueWidth� 	UnitWidthd  