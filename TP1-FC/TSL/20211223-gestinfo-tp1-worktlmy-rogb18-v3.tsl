//-------------------------------------------------
//-------------------------------------------------
//TSL Script para Modo Operativo en Texto de la Plataforma
//-------------------------------------------------
//-------------------------------------------------
tsl Tlmy::RODE.MOBC.Mode.Raw
return 2bits : mode_raw
label
-- SM Operation Mode Raw Data
description
-- Operational Mode Code in Binary/Raw Format
{
    //ExtractTlmyBits(integer: RefPosition, LSBit, Length)
	mode_raw = ExtractTlmyBits(30, 0, 2);
}

//-------------------------------------------------

tsl Tlmy::RODE.SMOBC.Mode.Int
return integer : mode_int
label
-- SM Operational Mode Integer Data
description
-- Operational Mode Code in Integer Format
{
    mode_int = RODE.MOBC.Mode.Raw;
}
//-------------------------------------------------

tsl Tlmy::RODE.SMOBC.Mode.Txt
return string : mode_txt
label
--Modo Operacional del SM: 
description
-- Operational Mode Code in Text Format:
-- Bin: Int: Text
-- XXXXXX00 : 0  : SCIENCE
-- XXXXXX01 : 1  : PROPULSION
-- XXXXXX10 : 2  : SURVIVAL
-- XXXXXX11 : 3  : SAFE HOLD
{
    if (RODE.SMOBC.Mode.Int == 0)
		mode_txt = "SCIENCE";
	else if (RODE.SMOBC.Mode.Int == 1)
		mode_txt="PROPULSION";
	else if (RODE.SMOBC.Mode.Int == 2)
		mode_txt="SURVIVAL";
	else if (RODE.SMOBC.Mode.Int == 3)
		mode_txt="SAFE HOLD";
	else
		mode_txt= "Error";
}

//-------------------------------------------------
//-------------------------------------------------
//TSL Script para identificar transmisor de bansa S operativo
//-------------------------------------------------
//-------------------------------------------------

tsl Tlmy::RODE.SMTXS.Active.Raw
return 2bits : out
label
-- SM active S-band transmitter Raw Data
description
-- Active S-band Transmitter Code in Binary/Raw Format
{
    out = ExtractTlmyBits(31, 0, 2);
}

//-------------------------------------------------

tsl Tlmy::RODE.SMTXS.Active.Int
return integer : out
label
--SM active S-band transmitter Integer Data
description
-- Active S-band Transmitter Code in Integer Format
{
    out = RODE.SMTXS.Active.Raw;
}

//-------------------------------------------------

tsl Tlmy::RODE.SMTXS.Active.Txt
return string : out
label
--Transmisor de Banda S: 
description
-- Active S-band Transmitter Code in Text Format:
-- Bin: Int: Text
-- XXXXXX00 : 0  : TXS1
-- XXXXXX01 : 1  : TXS2
-- XXXXXX10 : 2  : ERROR
-- XXXXXX11 : 3  : ERROR
{
    if (RODE.SMTXS.Active.Int == 0)
		out = "TXS1";
	else if (RODE.SMTXS.Active.Int == 1)
		out= "TXS2";
	else
		out= "ERROR";
}

//-------------------------------------------------
//-------------------------------------------------
//TSL Script para determinar el OBT en formato time
//-------------------------------------------------
//-------------------------------------------------

tsl Tlmy::RODE.SYS.OBT.Time.Extracted
//return  time : obt_ext
return  integer : obt_ext
label
-- OBT Extraido: 
description
-- On Board Time UTC format in Integer Format
local
  bytes : obt_raw;
{
    //OBT Extracted in Bytes
	//ExtractTlmyBytes(integer: RefPosition, Length)
	obt_raw = ExtractTlmyBytes(32, 4);
	//OBT to Integer from Raw 
	obt_ext= obt_raw;
}

//-------------------------------------------------
 
tsl Tlmy::RODE.SYS.OBT.Time.Referenced
return  time : obt_ref
//return integer: obt_ref_int
label
-- OBT UTC:  
description
--  On Board Time UTC format
local
  time : reftime;
  bytes : obt_raw;
  integer : obt_int;
{
    //OBT Extracted in Bytes
	obt_raw = ExtractTlmyBytes(32, 4);
	//OBT Extracted to Integer from Raw 
    obt_int= obt_raw;          
    //Addition the time in integer seconds to referenced time
	reftime= 1980/01/06 00:00:00;
	obt_ref= reftime + obt_int;
}

//-------------------------------------------------
//-------------------------------------------------
//TSL Script para determinar el tiempo del FC o CASTime
//-------------------------------------------------
//-------------------------------------------------

tsl Tlmy::RODE.SYS.CASTime.Time.WMSEC
return time : cas_time_wmsc
label
--Tiempo del FC con milisegundos:
description
-- CAS System Time in UTC format
{
    //Extracted CASTime with microseconds
	cas_time_wmsc = PDSLib::CAS.Time();
}

//-------------------------------------------------

tsl Tlmy::RODE.SYS.CASTime.Time.WOMSEC
return time : cas_time
label
--Tiempo del FC: 
description
-- CAS System Time in UTC format
local
  time: cas_raw;
  bytes : cas_all_bytes, cas_four_bytes;
{
	//Call the CASTime in time with microseconds
	cas_raw= RODE.SYS.CASTime.Time.WMSEC;
	
	//Option N°1: Extract de first four bytes
	//Convert CASTime in bytes from CASTime with microseconds
	//The total bytes is 8
	//The Most Significant 4 bytes is the elpased tiem in seconds since January 6th, 1980
	//The Less Significant 4 bytes is the remaining fraction of seconds in microseconds
	cas_all_bytes= cas_raw;
	//Extract the first 4 bytes (Most Significant 4 bytes) from CASTime in bytes
	cas_four_bytes = PDSLib::bytes.extract(cas_all_bytes,0,3);
	//Convert CASTime in UTC Time format from the Most Significant 4 bytes extracted
	cas_time= cas_four_bytes;
	
	//Option N°2: Using the OCL function Time.Truncate.MSecs(time : InValue)
	//cas_time= PDSLib::Time.Truncate.MSecs(cas_raw);
}

//-------------------------------------------------
//-------------------------------------------------
//TSL Script para determinar voltaje y alarma
//-------------------------------------------------
//-------------------------------------------------

tsl Tlmy::RODE.PMOBC.Batt.Volt.Int
return  integer : volt
label
-- Battery Voltage
description
-- Extracted Battery Bus Voltage in Real Format
local
  bytes : volt_extracted;
{
    volt_extracted = ExtractTlmyBytes(40, 2);
	volt = volt_extracted;
	//The value volt is it between 0 and 65535 (N= 2^16 - 1= 65535)
}

//-------------------------------------------------

tsl Tlmy::RODE.PMOBC.Batt.Volt.Real.Scaled
return real : volt_scaled
label
--Tension de Bus: <Unit>: "V" 
description
-- Scaled Battery Bus Voltage from Real Format to Limit Voltage
local
  real : slope;
{
	slope= 6553.5;
    volt_scaled = RODE.PMOBC.Batt.Volt.Int / slope;
	// Example: 
	// For PMOBC.Batt.Volt.Real= 65535, PMOBC.Batt.Volt.Real.Scaled= 65535 / 6553.5 = 10.0000V
	// For PMOBC.Batt.Volt.Real= 32768, PMOBC.Batt.Volt.Real.Scaled= 32768 / 6553.5 = 5.0001V
	// For PMOBC.Batt.Volt.Real= 0,     PMOBC.Batt.Volt.Real.Scaled= 0     / 6553.6 = 0.0000V
}

//-------------------------------------------------

tsl Alarm::RODE.PMOBC.Batt.Volt.Real.Scaled
return integer : volt_alarm
label
-- Battery Voltage Alarm
description
-- Battery Bus Voltage Alarm Analysis
local
  real : volt_alarm_min, volt_alarm_max, volt_aux;
{
	volt_alarm_min = 3.0;
	volt_alarm_max = 7.0;
	volt_aux = Tlmy::RODE.PMOBC.Batt.Volt.Real.Scaled;
    if ((volt_aux< volt_alarm_min) || (volt_aux > volt_alarm_max))
		volt_alarm= 2;
    else
		volt_alarm= 0;
	// The integer value 0= green = OK
	// The integer value 2= red   = Emergency
}

//-------------------------------------------------
//-------------------------------------------------
//TSL Script para el contador de frames de telemetría
//-------------------------------------------------
//-------------------------------------------------

tsl Tlmy::RODE.SMOBC.FrameCounter.Raw
return bytes : frame_count_raw
label
--Frame Counter Integer Data
description
-- Extracted Frame Counter in Raw Data Format
{
	frame_count_raw = ExtractTlmyBytes(6,3);
	//For the all frame, start count is: 0, end count is: 1023 (Nbytes= 1024)
	//The first 4 bytes is the ASM (start count: 0, end count: 3)
	//The next 2 bytes is the Master Channel ID (star count: 4, end count: 5)
	//The next 3 bytes is the Virtual Channel Frame Count (star count: 6, end count: 8)
	//So, from byte 6 to 8 corresponds to Frame Counter
}

//-------------------------------------------------

tsl Tlmy::RODE.SMOBC.FrameCounter.Int
return integer : frame_count_int
label
--Numero de Frame: 
description
-- Extracted Frame Counter in Integer Data Format
{
	frame_count_int = RODE.SMOBC.FrameCounter.Raw;
}

//-------------------------------------------------
//-------------------------------------------------