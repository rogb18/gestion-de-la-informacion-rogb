# NOMBRE DEL PROYECTO

GESTION DE LA INFORMACION-ROGB

# DESCRIPCIÓN

Proyecto para subir los trabajos prácticos de la matería Gestión de la Información.

# CONTENIDO

El contenido esta estructurado en los siguientes directorios:

## TP1-FC

Contiene los archivos generados para el TP1 sobre el uso de TSL, SCL y el Tlmy View.
Dentro están las siguientes carpetas:
- "Report": contiene un reporte del trabajo práctico
- "TMFrame": contiene los archivos .bin y .txt con las diez tramas de TM
- "TSL": contiene el archivo .tsl con los scripts generados para esta parte. También contiene el archivo .tsl y .tsh que se genero en clases para poder que el Tlmy View compilé correctamente
- "SCL": contiene el archivo .scl y .sch con los scripts generados para esta parte
- "TlmyView": contiene los archivos generados para la visualización de la TM en el Tlmy View

## TP2-XTCE

Contiene el archivo XML para validar con XSD

## TP3-Questions

Contiene el archivo pdf con el desarrollo del cuestionario de las Unidades 6 y 7

## Presentacion

Contiene el archivo pdf con la presentación de los trabajos prácticos

# AUTOR
Roderick González

